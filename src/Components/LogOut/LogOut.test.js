import React from "react";
import { shallow } from "enzyme";
import LogOut from "./LogOut";

describe("LogOut", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<LogOut />);
    });

    it("should render the LogOut", () => {
        expect(wrapper).toMatchSnapshot();
    });

    it("render the LogOut button", () => {
        expect(wrapper.find("#log-out-button").text()).toBe("Log Out");
    });

    it("should alert user with Logged out message, after clicking logOut button and confirming the logOut", () => {
        const alertSpy = jest.spyOn(window, 'alert')
        jest.spyOn(window, 'confirm').mockReturnValue(true);
        alertSpy.mockImplementation(() => { });
        wrapper.find("#log-out-button").simulate("click");
        expect(alertSpy).toHaveBeenCalledWith("Logged out")
    });

    it("shouldn't log user out after user canceled the prompt", () => {
        const alertSpy = jest.spyOn(window, 'alert')
        jest.spyOn(window, 'confirm').mockReturnValue(false);
        alertSpy.mockImplementation(() => { });
        wrapper.find("#log-out-button").simulate("click");
        expect(alertSpy).not.toHaveBeenCalled();
    });

    it("should prompt user from logging out", () => {
        const confirmSpy = jest.spyOn(window, 'confirm')
        confirmSpy.mockImplementation(() => { });
        wrapper.find("#log-out-button").simulate("click");
        expect(confirmSpy).toHaveBeenCalledWith("Are you sure you want to log out ?")
    });

    it("should prompt user from logging out", () => {
        const confirmSpy = jest.spyOn(window, 'confirm')
        confirmSpy.mockImplementation(() => { });
        wrapper.find("#log-out-button").simulate("click");
        expect(confirmSpy).toHaveBeenCalledWith("Are you sure you want to log out ?")
    });
});
