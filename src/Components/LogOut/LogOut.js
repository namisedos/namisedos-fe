import * as React from "react";
import Button from "@mui/material/Button";

export default function LogOut() {

    const handleLogOut = () => {
        if (window.confirm("Are you sure you want to log out ?")) {
            alert('Logged out');
            return;
        }
    }

    return (
        <div>
            <Button id="log-out-button" variant="outlined" onClick={handleLogOut}>
                Log Out
            </Button>
        </div>
    );
}
