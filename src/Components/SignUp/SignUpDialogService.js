import axios from "axios";

import { URL } from "../../api-config";

const registerUser = (data) => {
  return axios.post(`${URL}/User/CreateUser`, data);
};

export { registerUser };
