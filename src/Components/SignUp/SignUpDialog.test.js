import React from "react";
import { shallow } from "enzyme";
import SignUpDialog from "./SignUpDialog";

describe("SingUpDialog", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<SignUpDialog />);
  });

  describe("Counter Testing", () => {
    test("render the title of the signup button", () => {
      expect(wrapper.find("#SignUp-btn").text()).toBe("Sign Up");
    });
  });

  describe("handleClickOpen", () => {
    it("it should call setOpen with parameter true", () => {
      wrapper.find("#SignUp-btn").simulate("click");
      expect(wrapper.find("#SignUp-dialog").props().open).toBe(true);
    });
  });

  describe("handleClickClose cancel button", () => {
    it("it should call setOpen with parameter true", () => {
      wrapper.find("#cancel-btn").simulate("click");
      expect(wrapper.find("#SignUp-dialog").props().open).toBe(false);
    });
  });

  describe("handleClickClose signup button", () => {
    it("it should call setOpen with parameter true", () => {
      wrapper.find("#submit-signup").simulate("click");
      expect(wrapper.find("#SignUp-dialog").props().open).toBe(false);
    });
  });

  describe("validate password", () => {
    it("error should be true if password is not long enough", () => {
      wrapper.find("#SignUp-btn").simulate("click");
      wrapper.find("#username").simulate("change", {
        target: { name: "username", value: "username" },
      });
      wrapper.find("#name").simulate("change", {
        target: { name: "email", value: "abc@abc.com" },
      });
      wrapper.find("#password").simulate("change", {
        target: { name: "password", value: "password" },
      });
      wrapper.find("#n_password").simulate("change", {
        target: { name: "confirmPassword", value: "confirmPassword" },
      });
      expect(wrapper.find("#username").props().value).toBe("username");
      expect(wrapper.find("#name").props().value).toBe("abc@abc.com");
      expect(wrapper.find("#password").props().value).toBe("password");
      expect(wrapper.find("#n_password").props().value).toBe("confirmPassword");
    });
  });

  describe("validate password function", () => {
    it("validating the password", () => {
      wrapper.find("#SignUp-btn").simulate("click");
      wrapper.find("#password").simulate("change", {
        target: { name: "password", value: "12345" },
      });
      wrapper.find("#submit-signup").simulate("click");
    });
  });
});
