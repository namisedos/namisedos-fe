import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

export default function SingUpDialog() {
  const PASSWORD_LENGTH = 7;

  const [open, setOpen] = React.useState(false);
  const [data, setData] = React.useState({
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
  });
  const [errors, setErrors] = React.useState({
    short: "Password is required",
    mismatch: "",
    emailerr: "",
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleData = ({ target }) => {
    setData({ ...data, [target.name]: target.value });
  };

  const handleSubmit = () => {
    validatePassword();
    validateEmail();
  };

  const validateEmail = () => {
    let err = { ...errors };
    let { email } = data;
    if (email < 1) {
      err = {
        ...err,
        emailerr: "Email is needed",
      };
    }
    setErrors(err);
  };

  const validatePassword = () => {
    let error = false;
    const { password, confirmPassword } = data;
    let err = { ...errors };
    if (password && password.length < PASSWORD_LENGTH) {
      error = true;

      err = {
        ...err,
        short: "Password must contain at least 7 characters",
      };
    } else {
      err = {
        ...err,
        short: "",
      };
    }

    if (confirmPassword && confirmPassword !== password) {
      error = true;

      err = {
        ...err,
        mismatch: "Passwords don't match",
      };
    } else {
      err = {
        ...err,
        mismatch: "",
      };
    }
    setErrors(err);
    const { short, mismatch, emailerr } = err;
    if (short === "" && mismatch === "" && emailerr === "") {
      handleClose();
    }
  };

  return (
    <div>
      <Button id="SignUp-btn" variant="outlined" onClick={handleClickOpen}>
        Sign Up
      </Button>
      <Dialog id="SignUp-dialog" open={open} onClose={handleClose}>
        <DialogTitle>Sign Up</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="username"
            name="username"
            label="Username"
            type="text"
            fullWidth
            variant="standard"
            value={data.username}
            onChange={handleData}
          />
          <TextField
            autoFocus
            margin="dense"
            id="name"
            name="email"
            label="Email Address"
            type="email"
            fullWidth
            variant="standard"
            value={data.email}
            onChange={handleData}
            error={errors.emailerr !== ""}
            helperText={errors.emailerr}
          />
          <TextField
            autoFocus
            margin="dense"
            id="password"
            name="password"
            label="Password"
            type="password"
            fullWidth
            variant="standard"
            onChange={handleData}
            value={data.password}
            error={errors.short !== ""}
            helperText={errors.short}
          />
          <TextField
            autoFocus
            margin="dense"
            id="n_password"
            name="confirmPassword"
            label="Retype Password"
            type="password"
            fullWidth
            variant="standard"
            onChange={handleData}
            value={data.confirmPassword}
            error={Boolean(errors?.mismatch)}
            helperText={errors?.mismatch}
          />
        </DialogContent>
        <DialogActions>
          <Button id="cancel-btn" onClick={handleClose}>
            Cancel
          </Button>
          <Button id="submit-signup" onClick={handleSubmit}>
            SignUp
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
