import axios from "axios";

import { URL } from "../../api-config";

const loginUser = (user) => {
  return axios.post(`${URL}/User/Login`, user);
};

export { loginUser };
