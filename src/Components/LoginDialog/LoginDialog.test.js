import { shallow } from "enzyme";
import React from "react";
import LoginDialog from "./LoginDialog";
import { mount } from "enzyme";

describe("Login Dialog test", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<LoginDialog />);
  });

  it("should render the Login Dialog", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("render the Sign In button", () => {
    expect(wrapper.find("#sign-in-btn").text()).toBe("Sign In");
  });

  it("render the Cancel button", () => {
    expect(wrapper.find("#cancel-btn").text()).toBe("Cancel");
  });

  it("#dialog-open-btn opens sign-in-dialog", () => {
    wrapper.find("#dialog-open-btn").simulate("click");
    expect(wrapper.find("#sign-in-dialog").props().open).toBe(true);
  });

  it("#cancel-btn closes sign-in-dialog", () => {
    wrapper.find("#dialog-open-btn").simulate("click");
    wrapper.find("#cancel-btn").simulate("click");
    expect(wrapper.find("#sign-in-dialog").props().open).toBe(false);
  });

  it("user state should change according to the changes in the form", () => {
    wrapper.find("#dialog-open-btn").simulate("click");
    wrapper.find("#username").simulate("change", {
      target: { name: "username", value: "test" },
    });
    wrapper.find("#password").simulate("change", {
      target: { name: "password", value: "test" },
    });
    expect(wrapper.find("#username").props().value).toBe("test");
    expect(wrapper.find("#password").props().value).toBe("test");
  });

  it("when user fills the form and clicks on sign-in-btn, user should be logged in", () => {
    wrapper.find("#dialog-open-btn").simulate("click");
    wrapper.find("#username").simulate("change", {
      target: { name: "username", value: "test" },
    });
    wrapper.find("#password").simulate("change", {
      target: { name: "password", value: "test" },
    });

    wrapper.find("#sign-in-btn").simulate("click");

    setTimeout(() => {
      expect(wrapper.find("#sign-in-dialog").props().open).toBe(false);
    }, 1000);
  });
});
