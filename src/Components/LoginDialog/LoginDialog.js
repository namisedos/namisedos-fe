import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import * as React from "react";
import { useState } from "react";
import { loginUser } from "./LoginDialogService";

export default function LoginDialog() {
  const [open, setOpen] = useState(false);
  const [user, setUser] = useState({
    username: "",
    password: "",
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event) => {
    setUser({
      ...user,
      [event.target.name]: event.target.value,
    });
  };
  const handleLogin = async () => {
    if (user.username !== "" && user.password !== "") {
      await loginUser(user).then((response) => {
        if (response.status === 200) {
          handleClose();
        }
      });
    }
  };

  return (
    <div>
      <Button id="dialog-open-btn" variant="outlined" onClick={handleClickOpen}>
        Sign In
      </Button>
      <Dialog id="sign-in-dialog" open={open} onClose={handleClose}>
        <DialogTitle>Sign In</DialogTitle>
        <DialogContent>
          <FormControl>
            <TextField
              required
              autoFocus
              margin="dense"
              id="username"
              label="Username"
              type="text"
              fullWidth
              variant="standard"
              name="username"
              onChange={handleChange}
              value={user.username}
              error={user.username.length === 0}
              helperText={
                user.username === "" ? "Please enter a username!" : " "
              }
            />
            <TextField
              required
              margin="dense"
              id="password"
              label="Password"
              type="password"
              fullWidth
              variant="standard"
              name="password"
              value={user.password}
              onChange={handleChange}
              error={user.password.length === 0}
              helperText={
                user.password === "" ? "Please enter a password!" : " "
              }
            />
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button id="cancel-btn" onClick={handleClose}>
            Cancel
          </Button>
          <Button id="sign-in-btn" onClick={handleLogin}>
            Sign In
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
