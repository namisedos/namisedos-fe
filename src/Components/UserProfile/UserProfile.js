import * as React from "react";
import Button from "@mui/material/Button";
import Avatar from "@mui/material/Avatar"
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { deepPurple } from '@mui/material/colors';
import InputLabel from '@mui/material/InputLabel';
import TripsTable from './TripsTable'


export default function UserProfile({user}) {

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const getUserFirstLetter = ({ username }) => {
        return username.charAt(0);
    }


    return (
        <div >
            <Button id="open-view" variant="outlined" onClick={handleClickOpen}>
                <Avatar id="avatar" sx={{ bgcolor: deepPurple[500] }}>{getUserFirstLetter(user)}</Avatar>
            </Button>
            <Dialog id="view-user" open={open} onClose={handleClose}>
                <DialogTitle>Profile Info</DialogTitle>
                <DialogContent>
                    <div className="user-profile">
                    <div>
                        <InputLabel >Username</InputLabel>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="Username"
                            type="text"
                            fullWidth
                            variant="outlined"
                            disabled
                            value={user.username}
                        />
                    </div>
                    <div>
                        <InputLabel>Email</InputLabel>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="Username"
                            type="text"
                            fullWidth
                            variant="outlined"
                            disabled
                            value={user.email}
                        />
                    </div>

                    <div>
                        <InputLabel >Home Adress</InputLabel>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="Username"
                            type="text"
                            fullWidth
                            variant="outlined"
                            disabled
                            value={user.username}
                        />
                    </div>

                    <div>
                        <InputLabel htmlFor="component-helper">Trips</InputLabel>
                        <TripsTable trips={user.trips}/>
                    </div>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button id="close-view" onClick={handleClose}>Close</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
