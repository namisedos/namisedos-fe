import React from "react";
import { shallow } from "enzyme";
import UserProfile from "./UserProfile";

describe("UserProfile", () => {
    let wrapper;
    const user = {
        username: "test"
    }
    beforeEach(() => {
        wrapper = shallow(<UserProfile user={user}/>);
    });

    describe("getUserFirstLetter", () => {
        it("should return first letter of provided users name", () => {
            expect(wrapper.find("#view-user").text().includes('t'));
        });
    });

    describe("handleClickOpen", () => {
        it("it should call setOpen with parameter true", () => {
            wrapper.find("#open-view").simulate("click");
            expect(wrapper.find("#view-user").props().open).toBe(true);
        });
    });

    describe("handleClickClose cancel button", () => {
        it("it should call setOpen with parameter true", () => {
            wrapper.find("#close-view").simulate("click");
            expect(wrapper.find("#view-user").props().open).toBe(false);
        });
    });
});
