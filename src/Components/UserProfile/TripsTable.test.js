import React from "react";
import { shallow } from "enzyme";
import TripsTable from "./TripsTable";
import {TablePaginationActions} from "./TripsTable";
import { userFixture } from "../../Fixtures/UserFixture";

describe("TripsTable", () => {
    let wrapperTable;
    let wrapperPaginationActions;

    let count=3;
    let page=1;
    let rowsPerPage=1;
    let onPageChange;


    const trips = [userFixture.trips[0]]
    beforeEach(() => {
        onPageChange = jest.fn();
        wrapperTable = shallow(<TripsTable trips={trips} />);
        wrapperPaginationActions = shallow(<TablePaginationActions count={count}
            page={page}
            rowsPerPage={rowsPerPage}
            onPageChange={onPageChange}
           />);
    });

    it("should render the Login Dialog", () => {
        expect(wrapperTable).toMatchSnapshot();
        expect(wrapperPaginationActions).toMatchSnapshot();
    });


    describe("handleFirstPageButtonClick", () => {
        it("should call on page change with event and 0", () => {
            wrapperPaginationActions.find("#first-page-button").simulate("click");
            expect(onPageChange).toHaveBeenCalledWith(undefined,0);
        });
    });

    describe("handleBackButtonClick", () => {
        it("should call on page change with event and previous page number", () => {
            wrapperPaginationActions.find("#back-button").simulate("click");
            expect(onPageChange).toHaveBeenCalledWith(undefined, page-1);
        });
    });

    describe("handleNextButtonClick", () => {
        it("should call on page change with event and next page number", () => {
            wrapperPaginationActions.find("#next-button").simulate("click");
            expect(onPageChange).toHaveBeenCalledWith(undefined, page+1);
        });
    });

    describe("handleLastPageButtonClick", () => {
        it("should call on page change with event and middle page number", () => {
            wrapperPaginationActions.find("#last-page-button").simulate("click");
            expect(onPageChange).toHaveBeenCalledWith(undefined, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
        });
    });
});
