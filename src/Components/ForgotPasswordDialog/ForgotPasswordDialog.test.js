import { shallow } from "enzyme";
import React from "react";
import ForgotPasswordDialog from "./ForgotPasswordDialog";

describe("Login Dialog test", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<ForgotPasswordDialog />);
  });

  it("should render the Forgot Password Dialog", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("render the Forgot Password button", () => {
    expect(wrapper.find("#forgot-password-btn").text()).toBe(
      "Forgot Password?"
    );
  });

  it("render the Cancel button", () => {
    expect(wrapper.find("#cancel-btn").text()).toBe("Cancel");
  });

  it("pressing the Forgot Password button opens the dialog", () => {
    wrapper.find("#forgot-password-btn").simulate("click");
    expect(wrapper.find("#forgot-password-dialog").props().open).toBe(true);
  });

  it("pressing the Cancel button closes the dialog", () => {
    wrapper.find("#forgot-password-btn").simulate("click");
    wrapper.find("#cancel-btn").simulate("click");
    expect(wrapper.find("#forgot-password-dialog").props().open).toBe(false);
  });

  it("as an user i can provide email for which i want to reset my password", () => {
    wrapper.find("#forgot-password-btn").simulate("click");
    wrapper.find("#email").simulate("change", {
      target: { name: "email", value: "test@test.com" },
    });
    expect(wrapper.find("#email").props().value).toBe("test@test.com");
  });
});
