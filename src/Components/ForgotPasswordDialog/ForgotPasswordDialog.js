import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import * as React from "react";
import { useState } from "react";

export default function ForgotPasswordDialog() {
  const [open, setOpen] = useState(false);
  const [email, setEmail] = useState("");

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event) => {
    setEmail(event.target.value);
  };

  return (
    <div>
      <Button id="forgot-password-btn" onClick={handleClickOpen}>
        Forgot Password?
      </Button>
      <Dialog id="forgot-password-dialog" open={open} onClose={handleClose}>
        <DialogTitle>Forgot Your Password?</DialogTitle>
        <DialogContent>
          <DialogContentText>
            If you forgot your password, no worries: enter your email address
            and we'll send you a link you can use to pick a new password.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="email"
            label="Email Address"
            type="email"
            fullWidth
            variant="standard"
            name="email"
            value={email}
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button id="cancel-btn" onClick={handleClose}>
            Cancel
          </Button>
          <Button onClick={handleClose}>RESET PASSWORD</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
