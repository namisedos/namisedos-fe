import LoginDialog from "./Components/LoginDialog/LoginDialog";
import SingUpDialog from "./Components/SignUp/SignUpDialog";
import UserProfile from "./Components/UserProfile/UserProfile";
import { userFixture } from "./Fixtures/UserFixture";
import LogOut from "./Components/LogOut/LogOut";
function App() {
  return (
    <div>
      <LoginDialog />
      <SingUpDialog />
      <UserProfile user={userFixture}/>
      <LogOut/>
    </div>
  );
}
export default App;
