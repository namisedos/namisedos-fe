export const userFixture={
    id:"123",
    username:"test",
    email:"test@gmail.com",
    password:"passwordTest",
    trips:[
        {
        tripDate:"2022-04-03",
        carModel:"testCarModel",
        start:"testStartCoordinates",
        finish:"testEndCoordinates",
    },
        {
        tripDate:"2022-04-03",
        carModel:"testCarModel",
        start:"testStartCoordinates",
        finish:"testEndCoordinates",
    },
        {
        tripDate:"2022-04-03",
        carModel:"testCarModel",
        start:"testStartCoordinates",
        finish:"testEndCoordinates",
    },
        {
        tripDate:"2022-04-03",
        carModel:"testCarModel",
        start:"testStartCoordinates",
        finish:"testEndCoordinates",
    },
        {
        tripDate:"2022-04-03",
        carModel:"testCarModel",
        start:"testStartCoordinates",
        finish:"testEndCoordinates",
    },
],
    homeAdress:"testStreet",
    createdDate:"2015-05-16T05:50:06.7199222-04:00",
    updatedDate:"2015-05-17T05:50:06.7199222-04:00",
}