describe("LogOut", () => {
    it("should render the LogOut button", () => {
        cy.visit("/");
        cy.get("#log-out-button").should("be.visible");
    });

    it("should promp user from log out", () => {
        cy.visit("/");
        cy.get("#log-out-button").click();

        cy.on('window:confirmed', (str) => {
            expect(str).to.equal('Are you sure you want to log out ?')
        })
    });


    it("Should alert user with logged out after confirming the log out prompt", () => {
        cy.visit("/");

        cy.get("#log-out-button").click();

        cy.on('window:confirmed', (str) => {
            expect(str).to.equal('Are you sure you want to log out ?')
        })
        cy.on('window:confirm', () => true);

        cy.on('window:alert', (str) => {
            expect(str).to.equal('Logged out')
        })
    });

    it("User shouldn't get alerted after canceling the log out", () => {
        cy.visit("/");

        cy.get("#log-out-button").click();

        cy.on('window:confirmed', (str) => {
            expect(str).to.equal('Are you sure you want to log out ?')
        })
        cy.on('window:confirm', () => false);

        cy.on('window:alert', (str) => {
            expect(str).not.to.equal('Logged out')
        })
    });
});
