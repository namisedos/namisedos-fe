describe("Login Dialog test", () => {
  it("should render the Login Dialog", () => {
    cy.visit("/");
    cy.get("#dialog-open-btn").click();
    cy.get("#sign-in-dialog").should("be.visible");
  });

  it("render the Sign In button", () => {
    cy.visit("/");
    cy.get("#dialog-open-btn").click();
    cy.get("#sign-in-btn").should("be.visible");
  });

  it("render the Cancel button", () => {
    cy.visit("/");
    cy.get("#dialog-open-btn").click();
    cy.get("#cancel-btn").should("be.visible");
  });

  it("#dialog-open-btn opens sign-in-dialog", () => {
    cy.visit("/");
    cy.get("#dialog-open-btn").click();
    cy.get("#sign-in-dialog").should("be.visible");
  });

  it("#cancel-btn closes sign-in-dialog", () => {
    cy.visit("/");
    cy.get("#dialog-open-btn").click();
    cy.get("#cancel-btn").click();
    cy.get("#sign-in-dialog").should("not.exist");
  });

  it("user state should change according to the changes in the form", () => {
    cy.visit("/");
    cy.get("#dialog-open-btn").click();
    cy.get("#username").type("test");
    cy.get("#password").type("test");
    cy.get("#username").should("have.value", "test");
    cy.get("#password").should("have.value", "test");
  });

  it("when user fills the form and clicks on sign-in-btn, user should be logged in", () => {
    cy.visit("/");
    cy.get("#dialog-open-btn").click();
    cy.get("#username").type("test");
    cy.get("#password").type("test");
    cy.get("#sign-in-btn").click();
    cy.get("#sign-in-dialog").should("not.exist");
  });
});
